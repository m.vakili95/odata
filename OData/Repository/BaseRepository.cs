using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public abstract class BaseRepository<T> where T: class {
        protected DAL.CoreDbContext _dbContext;
        public BaseRepository(DAL.CoreDbContext dbContext) {
            this._dbContext = dbContext;
        }
        public T Add(T entity) {
            _dbContext.Add<T>(entity);
            return entity;
        }
        public T Edit(T entity) {
            _dbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public IQueryable<T> GetQueryable() {
            return _dbContext.Set<T>();
        }

        public IQueryable<T> GetQueryable(Expression<Func<T, bool>> match) {
            return _dbContext.Set<T>().Where(match);
        }

        public T Find(int id) {
            return _dbContext.Find<T>(id);
        }

        public Task<T> FindAsync(int id) {
            return _dbContext.FindAsync<T>(id);
        }

        public void Remove(T entity) {
            _dbContext.Remove<T>(entity);

        }

        public void Remove(int id) {
            _dbContext.Remove<T>(Find(id));
        }

        public void Save() {
            _dbContext.SaveChanges();
        }
        public Task SaveAsync() {
            return _dbContext.SaveChangesAsync();
        }

        
        
    }
}