using System.Linq;
using Models;
using Microsoft.EntityFrameworkCore;

namespace Repository {
    public class UserRepository: BaseRepository<User> {
        BookRepository _bookRepository;
        public UserRepository(DAL.CoreDbContext dbContext, BookRepository bookRepository): base(dbContext) {
            _bookRepository = bookRepository;
        }
    }
}