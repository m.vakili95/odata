using Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Repository {
    public class BookRepository: BaseRepository<Book> {
        public BookRepository(DAL.CoreDbContext dbContext): base(dbContext) {
        }

    }
}