﻿using System.Linq;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Models;
using Repository;

namespace OData.Controllers
{
    public class BooksController : ODataController
    {
        BookRepository _repository;
        public BooksController(BookRepository repository) {
            _repository = repository;
        }

        [ODataRoute("Books({id})")]
        [EnableQuery]
        [HttpGet]
        public IActionResult Find([FromODataUri] int id)
        {
            var data = _repository.Find(id);
            return Ok(data);
        }

        [EnableQuery]
        [HttpGet]
        public IActionResult Get()
        {
            var result = _repository.GetQueryable();
            return Ok(result);
        }

        [EnableQuery]
        [HttpPost]
        [Atomic]
        public IActionResult Post([FromBody] Book input) {
            var data = _repository.Add(input);
            return Ok(data);
        }

        [ODataRoute("Books({id})")]
        [EnableQuery]
        [HttpPut]
        [Atomic]
        public IActionResult Edit([FromODataUri] int id, [FromBody] Book input) {
            var data = _repository.Edit(input);
            return Ok(data);
        }

        [ODataRoute("Books({id})")]
        [EnableQuery]
        [HttpDelete]
        [Atomic]
        public IActionResult Delete([FromODataUri] int id) {
            _repository.Remove(id);
            return Ok();
        }
    }
}
