using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models {
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name {get; set;}
        public ICollection<Book> Books {get; set;}
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}