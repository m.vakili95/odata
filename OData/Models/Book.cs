using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models {
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Title {get; set;}
        [ForeignKey("Author")]
        public int? AuthorId {get; set;}
        public User Author {get; set;}
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}