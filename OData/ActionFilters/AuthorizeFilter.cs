using System.Diagnostics;
using System.Security.Claims;
using Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace core.ActionFilters
{

    public class AuthorizeClaim
    {
        public string Controller { get; set; }
        public string Action { get; set; }

        public AuthorizeClaim(string controller, string action)
        {
            Controller = controller;
            Action = action;
        }

    }
    public class CoreAuthorizeAttribute : TypeFilterAttribute
    {


        public CoreAuthorizeAttribute(string controller, string action) : base(typeof(CustomAuthorizeFilter))
        {
        }
    }



    public class CustomAuthorizeFilter : IAuthorizationFilter
    {
        readonly AuthorizeClaim _claim;

        public CustomAuthorizeFilter(AuthorizeClaim claim)
        {
            _claim = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
        }
    }
}