using Microsoft.AspNetCore.Mvc.Filters;
using DAL;
using System;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
public class AtomicAttribute : ActionFilterAttribute
{
    private bool _autoSave;

    public AtomicAttribute(bool autoSave = false) {
        this._autoSave = autoSave;
    }
 
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        var dbContext = (CoreDbContext) filterContext.HttpContext.RequestServices.GetService(typeof(CoreDbContext));
        dbContext.Database.BeginTransaction();
    }
 
    public override void OnActionExecuted(ActionExecutedContext filterContext)
    {
        var dbContext = (CoreDbContext) filterContext.HttpContext.RequestServices.GetService(typeof(CoreDbContext));

        if (_autoSave && dbContext.ChangeTracker.HasChanges()) {
            try {
                dbContext.SaveChanges();
            } catch {
                dbContext.Database.RollbackTransaction();
                return;
            }
        }
        
        dbContext.Database.CommitTransaction();
    }
 
}